package org.mathpiper.ui.gui.applications.voscilloscope.simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.event.MouseEvent;
import javax.imageio.ImageIO;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */


public abstract class DoubleRotateBitmap extends RotateBitmap
{

    public DoubleRotateBitmap(String FilesGrueso[], double ValuesGrueso[], String FilesFino[], double ValuesFino[])
    {
        super(FilesGrueso, ValuesGrueso);
        int i = 0;
        tracker = new MediaTracker(this);
        Image2 = new Image[FilesFino.length];
        try
        {
            for(i = 0; i < FilesFino.length; i++)
            {
                //Image2[i] = ImageIO.read(getClass().getResource(FilesFino[i]));
                Image2[i] = ImageIO.read(getClass().getClassLoader().getResourceAsStream("org/mathpiper/ui/gui/applications/voscilloscope/images/" + FilesFino[i].toLowerCase()));
                tracker.addImage(Image2[i], 0);
                tracker.waitForAll();
            }

        }
        catch(Exception e)
        {
            System.out.println("Error " + e.getMessage() + " loading image:" + FilesFino[i]);
        }
        PosValue = 0;
        PosImage2 = 0;
        Values = ValuesFino;
    }

    protected int newPosition(int x, int y, int centerX, int centerY)
    {
        int posAnt = PosValue % 44;
        double angulo = getAngle(x, y, centerX, centerY);
        int posNuevo = (int)(((double)22 * angulo) / 3.1415926535897931D);
        int inc = posNuevo - posAnt;
        if(inc > 22)
            inc = -44 + inc;
        if(inc < -22)
            inc = 44 + inc;
        return inc;
    }

    public abstract void btnMove(int i, int j);

    public void mouseClicked(MouseEvent e)
    {
        int x = e.getX();
        int y = e.getY();
        if(x > 55 && x < 55 + Image2[0].getWidth(this) && y > 28 && y < 28 + Image2[0].getHeight(this))
        {
            PosValue = PosValue + newPosition(x, y, 86, 43);
            if(PosValue < 0)
                PosValue = 0;
            else
            if(PosValue >= Values.length)
                PosValue = Values.length - 1;
            PosImage2 = PosValue % Image2.length;
            drawFino(getGraphics());
        } else
        if(x >= 0 && x <= getSize().width && y >= 0 && y <= getSize().height)
        {
            btnMove(x, y);
            draw(getGraphics());
        }
    }

    public void mouseDragged(MouseEvent e)
    {
        mouseClicked(e);
    }

    public Dimension getMinimumSize()
    {
        int width = super.Image[0].getWidth(this) + 20;
        int height = super.Image[0].getHeight(this) + 10;
        FontMetrics font = getFontMetrics(getFont());
        if(width < font.stringWidth(super.titulo))
            width = font.stringWidth(super.titulo);
        if(super.titulo.length() > 0)
            height += font.getHeight();
        return new Dimension(width, height);
    }

    public void paint(Graphics g)
    {
        g.setColor(getForeground());
        g.fillRoundRect(0, 0, getSize().width, getSize().height - 5, 20, 20);
        draw(g);
    }

    public void draw(Graphics g)
    {
        int width = getSize().width;
        int height = getSize().height;
        FontMetrics font = getFontMetrics(getFont());
        int y;
        if(super.titulo.length() > 0)
            y = font.getHeight();
        else
            y = 0;
        g.setColor(getBackground());
        g.fillOval((width / 2 - super.Image[0].getWidth(this) / 2) + 4, 14, super.Image[0].getWidth(this) - 8, super.Image[0].getHeight(this) - 8);
        g.setColor(Color.orange);
        g.drawString(super.titulo, width / 2 - font.stringWidth(super.titulo) / 2, height);
        g.drawImage(super.Image[super.posImage], width / 2 - super.Image[0].getWidth(this) / 2, 10, this);
        drawFino(g);
        drawValues(g);
    }

    public abstract void drawValues(Graphics g);

    public void drawFino(Graphics g)
    {
        g.drawImage(Image2[PosImage2], 55, 28, this);
    }

    public double getValueFino()
    {
        return Values[PosValue];
    }

    public void setValueFino(double value)
    {
        boolean esta = false;
        int i = 0;
        do
        {
            if(i >= Values.length || esta)
                break;
            if(value == Values[i])
            {
                esta = true;
                break;
            }
            i++;
        } while(true);
        if(esta)
        {
            PosValue = i;
            PosImage2 = i % Image2.length;
        } else
        {
            System.out.println("No exite ese value " + value + " en el componente fino de " + this);
        }
        repaint();
    }

    private MediaTracker tracker;
    protected Image Image2[];
    protected int PosImage2;
    protected double Values[];
    protected int PosValue;
}
