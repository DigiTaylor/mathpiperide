package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class SignalPhasePanelController extends GraphPanelController {
        private JSlider zoomSlider;
    private JSlider channel1PhaseSlider;
    private JSlider xGraphScaleSlider;
    private JSlider yGraphScaleSlider;
    
    private PlotPanel plotPanel;
    
    public SignalPhasePanelController(final ViewPanel viewPanel, double initialValue) {
        super(viewPanel, initialValue);
        
        plotPanel = (PlotPanel) viewPanel;

        channel1PhaseSlider = new JSlider(JSlider.HORIZONTAL,  0, 360, 0);
        channel1PhaseSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                JSlider source = (JSlider) e.getSource();

                int intValue = (int) source.getValue();
                PlotPanel plotPanel = (PlotPanel) viewPanel;
                plotPanel.setChannel1Phase(intValue);
                plotPanel.repaint();
            }
        });
        
        channel1PhaseSlider.setPaintLabels(true);
        channel1PhaseSlider.setPaintTicks(true); 
 	channel1PhaseSlider.setMajorTickSpacing(90); 
 	channel1PhaseSlider.setMinorTickSpacing(30);
        channel1PhaseSlider.setPreferredSize(new Dimension(channel1PhaseSlider.getPreferredSize().width, channel1PhaseSlider.getPreferredSize().height + 40));
        JPanel sliderPanel = new JPanel();
        sliderPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        sliderPanel.add(new JLabel("Phase"));
        sliderPanel.add(channel1PhaseSlider);
        this.add(sliderPanel);
    }
}
