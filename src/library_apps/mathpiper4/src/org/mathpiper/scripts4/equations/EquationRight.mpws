%mathpiper,def="EquationRight"

EquationRight(symbolicEquation_)::(Type(symbolicEquation) =? "==")  <--
{
    Local(listForm);
    
    listForm := ProcedureToList(symbolicEquation);
    
    listForm[3];
}

%/mathpiper



%mathpiper_docs,name="EquationRight",categories="Mathematics Procedures,Expression Manipulation"
*CMD EquationRight --- return the right side of a symbolic equation
*STD
*CALL
        EquationRight(equation)
        
*PARMS

{equation} -- symbolic equation.


*DESC

A symbolic equation is an equation which is defined using the == operator.  This
procedure returns the right side of a symbolic equation.

*E.G.


In> equ := y^2 == 4*p*x
Result: y^2==4*p*x


In> EquationRight(equ)
Result: 4*p*x

*SEE ==, EquationLeft
%/mathpiper_docs
