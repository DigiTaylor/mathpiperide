%mathpiper,def="LocalCreate"

LocalCreate() :=
{
    Local(stringList, entry, entries, entriesLength, variableName, index);
    
    stringList := ["Local("];
    
    entries := State();
    
    entriesLength := Length(entries);
    
    index := 1;

    While(index <=? entriesLength)
    {
        entry := entries[index];
        
        variableName := ToString(entry[1]);
        
        If(StringMidGet(1, 1, variableName) !=? "?")
        {
            Append!(stringList, variableName);
            
            If(index <? entriesLength)
            {
                Append!(stringList, ", ");
            }
        }
        
        index++;
    }
    
    
    Append!(stringList, ");");
    
    Echo(ListToString(stringList, ""));
}

%/mathpiper






%mathpiper_docs,name="LocalCreate",categories="Programming Procedures,Variables"
*CMD LocalCreate --- create a string that contains a call to the procedure "Local" 
*CALL
    LocalCreate()

    
*DESC
This procedure is used to create a call to the procedure "Local" using all of the global
variables that are currently assigned as arguments. It is useful for adding a call to 
the procedure "Local" as the last part of the following procedure definition process:

1) Define the procedure.
2) Test the procedure for correct operation.
3) Unassign all global variables.
4) Call the procedure once.
5) Call LocalCreate().
6) Paste the generated call to the procedure "Local" into the top of the procedure definition.


*E.G.
In> Unassign(All)
Result: True

In> arg1 := 3
Result: 3

In> arg2 := 5
Result: 5

In> State()
Result: [arg1:3,arg2:5]

In> LocalCreate()
Result: True
Side Effects:
Local(arg2, arg1);
%/mathpiper_docs





