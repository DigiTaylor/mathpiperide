%mathpiper,def="NodePathDistance"

NodePathDistance(node1Position, node2Position) :=
{
    Local(distance, shortest, index, node1PositionList, node2PositionList);
     
    node1PositionList := PositionStringToList("0," + node1Position);
    
    node2PositionList := PositionStringToList("0," + node2Position);
    
    distance := 0;
    
    shortest := Decide(Length(node2PositionList) <? Length(node1PositionList),  Length(node2PositionList), Length(node1PositionList));

    index := 1;

    While(index <=? shortest)
    {
        If(node2PositionList[index] !=? node1PositionList[index])
        {
            Break();
        }

        index++;
    }

    index--;
    
    distance := (Length(node1PositionList) - index) + (Length(node2PositionList) - index);
}

%/mathpiper





%mathpiper_docs,name="NodePathDistance",categories="Programming Procedures,Expression Trees"
*CMD NodePathDistance --- determine the path distance between two nodes

*CALL
    NodePathDistance(node1Position, node2Position)

*PARMS

{node1Position} -- position of the first node

{node2Position} -- position of the second node


*DESC

Determine the path distance between two nodes.

*E.G.
In> NodePathDistance("1,1","1,2,2") 
Result: 3

*SEE SymbolPathDistanceTotal
%/mathpiper_docs





%mathpiper,name="NodePathDistance",subtype="automatic_test"

Verify(NodePathDistance("1,1","1,2,2"), 3);
Verify(NodePathDistance("1,1","1,2,1,2"), 4);
Verify(NodePathDistance("1,1","2"), 3);
Verify(NodePathDistance("1","1,2,1,2"), 3);
Verify(NodePathDistance("1", "1"), 0);

%/mathpiper