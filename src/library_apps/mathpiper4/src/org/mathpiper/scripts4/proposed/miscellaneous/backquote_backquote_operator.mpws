%mathpiper,def="``"

Procedure("``", ["body"])
{
    Local(toFlattenPositions, backBodyString);
    toFlattenPositions := PositionsPattern(body, '@@q_);
    
    body := Substitute('@@, '@) body;
    
    body := Eval(ListToProcedure([ToAtom("`"), ListToProcedure([ToAtom("'"), body]) ]));
    
    toFlattenPositions := Sort(toFlattenPositions, Lambda([lhs, rhs], 
        {
            Local(lhsList, rhsList);
            
            If (lhs =? "") { True; }
            Else If (rhs =? "") { False; }
            Else
            {
                lhsList := PositionStringToList(lhs);
                rhsList := PositionStringToList(rhs);
                
                If (Length(lhsList) !=? Length(rhsList)) 
                { 
                    Length(lhsList) >? Length(rhsList); 
                }
                Else
                {
                    PopBack(lhsList) >? PopBack(rhsList);
                }
            }
        }
    ));
    
    ForEach(position, toFlattenPositions) 
    {
        Local(list, parent, parentPosition, parentExpression, locationInParent);
        
        If (position =? "") 
        {
            ExceptionThrow("Invalid Argument",
                "Invalid argument for ``, @@ cannot be root.");
        }
        
        parentPosition := PositionStringToList(position);
        locationInParent := PopBack(parentPosition);
        parentPosition := ListToString(parentPosition, ",");
        
        list := PositionGet(body, position);
        parent := PositionGet(body, parentPosition);
        
        If (!? List?(list)) 
        {
            ExceptionThrow("Invalid Argument",
                "Invalid argument for @@, must be a list. " + 
                "At tree location: " + position);
        }
        
        parent := ProcedureToList(parent);
                                       
        Delete!(parent, locationInParent + 1);
       
        ForEach(item, list)
        {
            Insert!(parent, locationInParent+1, item);
            locationInParent +:= 1;
        }
        
        parent := ListToProcedure(parent);
        
        SubstitutePosition(body, parentPosition, 'True, parent);
    }
    
    Eval(body);
}

UnFence("``", 1);
HoldArgumentNumber("``", 1, 1);

%/mathpiper


%mathpiper_docs,name="``",categories="Operators"
*CMD `` --- list injection

*CALL
        `` [..., @@aList, ...]
        `` {...; @@aList; ...;}

*PARMS

{aList} -- list to inject

*DESC

Any variable prefixed by "@@" is first substituted into an expression using {`} macro substitution,
and then is 'pulled' into the parent container, instead of staying as a list its conents become a part
of the container. 

Any variable prefixed by "@" is substituted into an expression using normal {`} macro substitution.

This interacts well with MathPiper's Blank Sequence pattern matching, allowing complex substitutions to procedures to be obtained

*E.G.
In> aList := [1, 2, 3]
Result: [1,2,3]

Note the difference between ` and `` in the following examples

In> ` [0, @aList]
Result: [0,[1, 2, 3]]

In> `` [0, @@aList]
Result: [0,1,2,3]


The same operator works with a List as a parent and a Sequence

In> bList := [Hold(index--), Hold(Echo(index))]
Result: [index--,Echo(index)]

In> `` '{index := 10; @@bList;}
Result: {
  index := 10;

  index--;

  Echo(index);
}


The following uses `` to define Sequence concatenation globally:

In> {a___;} + {b___;} <-- `` ' {@@a; @@b;}
Result: True

In> aSequence := '{index := 10; index++;}
Result: {
  index := 10;

  index++;
}

In> bSequence := '{index--; Echo(index);}
Result: {
  index--;

  Echo(index);
}

In> aSequence + bSequence
Result: {
  index := 10;

  index++;

  index--;

  Echo(index);
}


Finally, converting a common While loop into a For loop. Note how `` interacts with @ as well as @@

In> code := '{index := 10; Echo(index); While(index <=? 20) { Echo(index); index++; } Echo(index);}
Result: {
  index := 10;

  Echo(index);

  While(index <=? 20)
  {
    Echo(index);

    index++;
  }

  Echo(index);
}

In> whileMatchRule := '{a___; predicateVar_ := initValue_Number?; b___; While(predicateVar_ <=? endValue_Number?) {c___; increment_;} d___;}
Result: {
  a___;

  predicateVar_ := initValue_Number?;

  b___;

  While(predicateVar_ <=? endValue_Number?)
  {
    c___;

    increment_;
  }

  d___;
}

In> SubstitutePosition(code, "", whileMatchRule, ' `` ' {@@a; @@b; For(@predicateVar := @initValue, @predicateVar <=? @endValue, @increment) { @@c; } @@d; })
Result: {
  Echo(index);

  For(index := 10,index <=? 20,index++ )
  {
    Echo(index);
  }

  Echo(index);
}

*SEE `
%/mathpiper_docs

%mathpiper,name="``",subtype="automatic_test"
Local(aList);

aList := [1, 2, 3];

Verify(`` [0, @aList], [0, [1, 2, 3]]);
Verify(`` [0, @@aList], [0, 1, 2, 3]);

Verify(`` '{foo; @@aList;}, '{foo; 1; 2; 3;});

aList := [];

Verify(`` [@@aList], []);

Verify(
    SubstitutePosition('{index := 10; index++; Echo(index);}, "", '{a___; Echo(b___); c___;}, ' `` '{@@a; @@c;}),
    '{index := 10; index++;});
%/mathpiper

