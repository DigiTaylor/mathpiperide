%mathpiper,name="lists",subtype="automatic_test"

Verify(Intersection('[aa,b,c],'[b,c,d]),'[b,c]);
Verify(Union('[aa,b,c],'[b,c,d]),'[aa,b,c,d]);
Verify(Difference('[aa,b,c],'[b,c,d]),'[aa]);

NextTest("VarList");
Verify(VarList('(x^2 + y^3)) , '[x, y]);
Verify(List(1,2,3),[1 , 2 , 3]);

Testing("BubbleSort");
Verify(BubbleSort([2,3,1],"<?"),[1,2,3]);
Verify(BubbleSort([2,3,1],">?"),[3,2,1]);

Testing("HeapSort");
Verify(HeapSort([2,3,1],"<?"),[1,2,3]);
Verify(HeapSort([2,1,3],">?"),[3,2,1]);
Verify(HeapSort([7,3,1,2,6],"<?"),[1,2,3,6,7]);
Verify(HeapSort([6,7,1,3,2],">?"),[7,6,3,2,1]);

Verify(Type(Sqrt(_x)),"Sqrt");
Verify(ArgumentsCount(Sqrt(_x)),1);
Verify(Contains?([_a,_b,_c],_b),True);
Verify(Contains?([_a,_b,_c],_d),False);

Verify(Append([_a,_b,_c],_d),[_a,_b,_c,_d]);
Verify(RemoveDuplicates([_a,_b,_b,_c]),[_a,_b,_c]);
Verify(Count([_a,_b,_b,_c],_b),2);
Verify(VarList('(x*Sqrt(x))), '[x]);


{
  Local(l);
  l:=[1,2,3];
  Delete!(l,1);
  Verify(l,[2,3]);
  Insert!(l,1,1);
  Verify(l,[1,2,3]);
  l[1] := 2;
  Verify(l,[2,2,3]);
  l[1] := 1;
  Delete!(l,3);
  Verify(l,[1,2]);
  Insert!(l,3,3);
  Verify(l,[1,2,3]);
  Delete!(FlatCopy(l),1);
  Verify(l,[1,2,3]);
}

Verify(BuildList(Factorial(i),i,1,4,1),[1,2,6,24]);
Verify(PermutationsList([_a,_b,_c]),[[_a,_b,_c],[_a,_c,_b],[_c,_a,_b],[_b,_a,_c],[_b,_c,_a],[_c,_b,_a]]);

Testing("ListOperations");
Verify(First('[a,b,c]),'a);
Verify(Rest('[a,b,c]),'[b,c]);
Verify(Reverse!('[a,b,c]),'[c,b,a]);
Verify(ListToProcedure('[a,b,c]),'(a(b,c)));
Verify(ProcedureToList('(a(b,c))),'[a,b,c]);

Verify(Delete([_a,_b,_c],2),[_a,_c]);
Verify(Insert([_a,_c],2,_b),[_a,_b,_c]);

Testing("Length");
Verify(Length([_a,_b]),2);
Verify(Length([]),0);

Testing("Nth");
Verify(Nth([_a,_b],1),_a);
Verify([_a,_b,_c][2],_b);

Testing("Concat");
Verify(Concat([_a,_b],[_c,_d]),[_a,_b,_c,_d]);
//This is simply not true!!! Verify(Hold(Concat([a,b],[c,d])),Concat([a,b],[c,d]));

/* todo:tk:commenting out for minimal version of the scripts.
Testing("Binary searching");
Verify(BSearch(100,[[n],n^2-15]), -1);
Verify(BSearch(100,[[n],n^2-16]), 4);
Verify(BSearch(100,[[n],n^2-100002]), -1);
Verify(BSearch(100,[[n],n^2-0]), -1);
Verify(FindIsq(100,[[n],n^2-15]), 3);
Verify(FindIsq(100,[[n],n^2-16]), 4);
Verify(FindIsq(100,[[n],n^2-100002]), 100);
Verify(FindIsq(100,[[n],n^2-0]), 1);
*/

Verify(Difference(ProcedureList(_a*_b/_c*_d), ["*","/"]), []);
//Verify(Difference(ProcedureListArithmetic(0*_x*Sin(_a/_b)*Ln(Cos(_y-_z)+Sin(_a))), [ToAtom("*"),'Ln,'Sin]), []);
Verify(Difference(VarListArith(_x+_a*_y^2-1), [_x,_a,_y^2]), []);

//Verify(Difference(ProcedureList(CUnparsable?({i:=0;While(i<?10){i++; a--; a:=a+i; [];}})), ['CUnparsable?,'Sequence,:=,'While,<?,++,--,ToAtom("+"),'List]), []);
Verify(ProcedureList([1,2,3]),["List"]);
Verify(ProcedureList([[],[]]),["List"]);
Verify(ProcedureList([]),["List"]);

Testing("AssociationDelete");
{
  Local(hash);
  hash:=[["A",1],["A",2],["B",3],["B",4]];
  AssociationDelete(hash,["B",3]);
  Verify(hash, [["A",1],["A",2],["B",4]]);
  Verify(AssociationDelete(hash,"A"),True);
  Verify(hash, [["A",2],["B",4]]);
  Verify(AssociationDelete(hash,"C"),False);
  Verify(hash, [["A",2],["B",4]]);
  AssociationDelete(hash,"A");
  Verify(hash, [["B",4]]);
  AssociationDelete(hash, ["A",2]);
  AssociationDelete(hash,"A");
  Verify(hash, [["B",4]]);
  Verify(AssociationDelete(hash,"B"),True);
  Verify(hash, []);
  Verify(AssociationDelete(hash,"A"),False);
  Verify(hash, []);
}
Testing("-- Arithmetic Operations");
Verify(1+[3,4],[4,5]);
Verify([3,4]+1,[4,5]);
Verify([1]+[3,4],Hold([1]+[3,4]));
Verify([3,4]+[1],Hold([3,4]+[1]));
Verify([1,2]+[3,4],[4,6]);
Verify(1-[3,4],[-2,-3]);
Verify([3,4]-1,[2,3]);
Verify([1]-[3,4],Hold([1]-[3,4]));
Verify([3,4]-[1],Hold([3,4]-[1]));
Verify([1,2]-[3,4],[-2,-2]);
Verify(2*[3,4],[6,8]);
Verify([3,4]*2,[6,8]);
Verify([2]*[3,4],Hold([2]*[3,4]));
Verify([3,4]*[2],Hold([3,4]*[2]));
Verify([1,2]*[3,4],[3,8]);
Verify(2/[3,4],[2/3,1/2]);
Verify([3,4]/2,[3/2,2]);
Verify([2]/[3,4],Hold([2]/[3,4]));
Verify([3,4]/[2],Hold([3,4]/[2]));
Verify([1,2]/[3,4],[1/3,1/2]);
Verify(2^[3,4],[8,16]);
Verify([3,4]^2,[9,16]);
Verify([2]^[3,4],Hold([2]^[3,4]));
Verify([3,4]^[2],Hold([3,4]^[2]));
Verify([1,2]^[3,4],[1,16]);

// non-destructive Reverse operation
{
  Local(lst,revlst);
  lst:=[_a,_b,_c,13,19];
  revlst:=Reverse(lst);
  Verify(revlst,[19,13,_c,_b,_a]);
  Verify(lst,[_a,_b,_c,13,19]);
}
Verify(Assigned?(lst),False);
Verify(Assigned?(revlst),False);

%/mathpiper