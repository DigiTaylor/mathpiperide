%mathpiper,def="Count"

Procedure("Count",["list", "element"])
{
   Local(result);
   Assign(result,0);
   ForEach(item,list) Decide(Equal?(item, element), Assign(result,AddN(result,1)));
   result;
}

%/mathpiper



%mathpiper_docs,name="Count",categories="Programming Procedures,Lists (Operations)"
*CMD Count --- count the number of occurrences of an expression
*STD
*CALL
        Count(list, expr)

*PARMS

{list} -- the list to examine

{expr} -- expression to look for in "list"

*DESC

This command counts the number of times that the expression "expr"
occurs in "list" and returns this number.

*E.G.

In> lst := [_a,Pi,3,1,Pi,II];
Result: [_a,Pi,3,1,Pi,II]

In> Count(lst,Pi)
Result: 2

In> Count(lst, 1);
Result: 1;

In> Count(lst, _x);
Result: 0;

*SEE Length, Select, Contains?
%/mathpiper_docs