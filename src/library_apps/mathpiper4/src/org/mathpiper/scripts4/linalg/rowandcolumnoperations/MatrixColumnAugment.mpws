%mathpiper,def="MatrixColumnAugment"

MatrixColumnAugment( M_Matrix?, v_Vector? )::(Length(v)=?Dimensions(M)[2]) <--
{
    Local(mRows,nCols,newMat,ir);
    Local(MT,MA);
    MT := Transpose(M);
    MT := MatrixRowStack(MT,v);
    MA := Transpose(MT);
}

%/mathpiper





%mathpiper_docs,name="MatrixColumnAugment",categories="Mathematics Procedures,Linear Algebra"
*CMD MatrixColumnAugment --- Adds a column to the end of the matrix
*STD
*CALL
        MatrixColumnAugment(m,vec)

*PARMS

{m} -- Matrix
{vec} -- vector of values to put in the column

*DESC

This command will add a column to the end of a matrix. The modified matrix is returned.

*E.G.
In> aa := BuildMatrix(Lambda([m,n],m*n),3,3)
Result: [[1,2,3],[2,4,6],[3,6,9]]

In> MatrixColumnAugment(aa,[7,8,9])
Result: [[1,2,3,7],[2,4,6,8],[3,6,9,9]]
%/mathpiper_docs
