%mathpiper,def="MatrixRowReplace"

MatrixRowReplace( M_Matrix?, iRow_PositiveInteger?, v_Vector? )::(Length(v)=?Length(M[1])) <--
{
    Local(mRows,nCols);
    [mRows,nCols] := Dimensions(M);
    Decide( iRow <=? mRows, Replace!(M,iRow,v) );
    M;
}

%/mathpiper

%mathpiper_docs,name="MatrixRowReplace",categories="Mathematics Procedures,Linear Algebra"
*CMD MatrixRowReplace --- Replace a row in a matrix
*STD
*CALL
        MatrixRowReplace(m,row,vec)

*PARMS

{m} -- Matrix
{row} -- row to replace(as an integer)
{vec} -- vector of values to put in the row

*DESC

This command will replace the specified row of a matrix with a vector you pass in. The modified matrix is returned.

*E.G.
In> aa := BuildMatrix(Lambda([m,n],m*n),3,3)
Result: [[1,2,3],[2,4,6],[3,6,9]]

In> MatrixRowReplace(aa,1,[7,8,9])
Result: [[7,8,9],[2,4,6],[3,6,9]]
%/mathpiper_docs