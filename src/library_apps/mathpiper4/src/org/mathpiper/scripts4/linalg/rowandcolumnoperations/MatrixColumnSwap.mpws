%mathpiper,def="MatrixColumnSwap"

MatrixColumnSwap( M_Matrix?, jCol1_PositiveInteger?, jCol2_PositiveInteger? )::
                          (And?(jCol1<=?Dimensions(M)[2],jCol2<=?Dimensions(M)[2])) <--
{
    Local(MT);
    MT := Transpose(M);
    MT := MatrixRowSwap(MT,jCol1,jCol2);
    M  := Transpose(MT);
}

%/mathpiper




%mathpiper_docs,name="MatrixRowSwap",categories="Mathematics Procedures,Linear Algebra"
*CMD MatrixColumnSwap --- Swaps two different columns of a matrix
*STD
*CALL
        MatrixColumnSwap(m,col1,col2)

*PARMS

{m} -- Matrix
{col1} -- the first column to swap
{col2} -- the second column to swap

*DESC

This command will swap all the elements in the two specified columns.

*E.G.
In> aa := BuildMatrix(Lambda([m,n],m*n),3,3)
Result: [[1,2,3],[2,4,6],[3,6,9]]

In> MatrixColumnSwap(aa,1,3)
Result: [[3,2,1],[6,4,2],[9,6,3]]
%/mathpiper_docs
