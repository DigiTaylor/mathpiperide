%mathpiper,def="Sqrt"

0 ## Sqrt(0) <-- 0;
0 ## Sqrt(Infinity) <--  Infinity;
0 ## Sqrt(-Infinity) <-- Complex(0,Infinity);
0 ## Sqrt(Undefined) <--  Undefined;
1 ## Sqrt(x_PositiveInteger?)::(Integer?(SqrtN(x))) <-- SqrtN(x);
2 ## Sqrt(x_PositiveNumber?)::NumericMode?() <-- SqrtN(x);
2 ## Sqrt(x_NegativeNumber?) <-- Complex(0,Sqrt(-x));
/* 3 ## Sqrt(x_Number?/y_Number?) <-- Sqrt(x)/Sqrt(y); */
3 ## Sqrt(x_Complex?)::NumericMode?() <-- x^(1/2);
/* Threading  */
Sqrt(xlist_List?) <-- MapSingle("Sqrt",xlist);

90 ## (Sqrt(x_Constant?))::(NegativeNumber?(NM(x))) <-- Complex(0,Sqrt(-x));

110 ## Sqrt(Complex(r_, i_)) <-- Exp(Ln(Complex(r,i))/2);


400 ## Sqrt(x_Integer?)::Integer?(SqrtN(x)) <-- SqrtN(x);
400 ## Sqrt(x_Integer?/y_Integer?)::(Integer?(SqrtN(x)) &? Integer?(SqrtN(y))) <-- SqrtN(x)/SqrtN(y);

%/mathpiper



%mathpiper_docs,name="Sqrt",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD Sqrt --- square root
*STD
*CALL
        Sqrt(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure calculates the square root of "x". If the result is
not rational, the call is returned unevaluated unless a numerical
approximation is forced with the {NM} procedure. This
procedure can also handle negative and complex arguments.

This procedure is threaded, meaning that if the argument {x} is a
list, the procedure is applied to all entries in the list.

*E.G.

In> Sqrt(16)
Result: 4;

In> Sqrt(15)
Result: Sqrt(15);

In> NM(Sqrt(15))
Result: 3.8729833462;

In> Sqrt(4/9)
Result: 2/3;

In> Sqrt(-1)
Result: Complex(0,1);

*SEE Exp, ^, NM
%/mathpiper_docs





%mathpiper,name="Sqrt",subtype="automatic_test"

Verify(Sqrt(Infinity),Infinity);

// version 1.0.56: Due to BitCount returning negative values sometimes, procedures depending on
// proper procedureing failed. MathSqrtFloat failed for instance on NM(1/2). It did give the right
// result for 0.5.
NumericEqual(NM(Sqrt(500000e-6),20),NM(Sqrt(0.0000005e6),20),20);
NumericEqual(NM(Sqrt(0.5),20),NM(Sqrt(NM(1/2)),20),20);

%/mathpiper