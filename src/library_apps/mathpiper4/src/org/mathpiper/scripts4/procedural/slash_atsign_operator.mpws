%mathpiper,def="/@"

Procedure("/@",["func","lst"]) Apply("MapSingle",[func,lst]);

%/mathpiper



%mathpiper_docs,name="/@",categories="Operators"
*CMD /@ --- apply a procedure to all entries in a list
*STD
*CALL
        fn /@ list

*PARMS

{fn} -- procedure to apply

{list} -- list of arguments

*DESC
This procedure is a shorthand for {MapSingle}. It
successively applies the procedure "fn" to all the entries in
"list" and returns a list contains the results. The parameter "fn"
can either be a string containing the name of a procedure or a pure
procedure.

This operator can help the user to program in the style of functional 
programming languages such as Miranda or Haskell.

*E.G.

In> "Sin" /@ [_a,3]
Result: [Sin(_a),Sin(3)];

In> Sin /@ [2,4,NM(Pi,16), Pi]
Result: [Sin(2),Sin(4),Sin(3.141592653589793),Sin(Pi)]

In> SinN /@ [2,4,NM(Pi,16)]
Result: [0.9092974269,-0.7568024961,0.0000000003906125890]

In> Lambda([a],Sin(a)*a) /@ [_a,4]
Result: [Sin(_a)*_a,4*Sin(4)]


*SEE MapSingle, Map, MapArgs
%/mathpiper_docs