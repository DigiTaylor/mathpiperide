/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp.parametermatchers;

import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;

/// Class for matching against a list of PatternParameterMatcher objects.
public class SublistPatternParameterMatcher extends SingularPatternParameterMatcher {

    protected PatternParameterMatcher iRootMatcher;

    protected int iNumberOfMatchers;


    public SublistPatternParameterMatcher(PatternParameterMatcher aRootMatcher, int aNrMatchers) {
        iRootMatcher = aRootMatcher;
        iNumberOfMatchers = aNrMatchers;
    }

    @Override
    protected boolean doMatch(Environment aEnvironment, int aStackTop, Cons aExpression, Cons[] arguments) throws Throwable {

        if (!(aExpression.car() instanceof Cons)) {
            return false;
        }

        Cons consTraverser = (Cons) aExpression.car();
        
        return iRootMatcher.argumentMatches(aEnvironment, aStackTop, consTraverser, arguments);
        
        /*
        for (int i = 0; i < iNumberOfMatchers; i++) {

            if (consTraverser == null) {
                return false;
            }
            
            if (!iMatchers[i].doMatch(aEnvironment, aStackTop, consTraverser, arguments)) {
                return false;
            }

            consTraverser = consTraverser.cdr();
        }

        if (consTraverser != null) {
            return false;
        }
        */
    }

    @Override
    public String getType()
    {
        return "Sublist";
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        PatternParameterMatcher matcher = iRootMatcher;

        for(int x = 0; x < iNumberOfMatchers; x++)
        {
            stringBuilder.append(matcher.getType());
            
            stringBuilder.append(": ");
            
            stringBuilder.append(matcher.toString());

            stringBuilder.append(", ");
            
            matcher = matcher.getNextMatcher();
            
        }

        return stringBuilder.toString();
    }

}
