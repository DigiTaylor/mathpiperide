%mathpiper,def="EvenFunction?"

EvenFunction?(f,x):= (f =? Eval(Substitute(x,-x)f));

%/mathpiper



%mathpiper_docs,name="EvenFunction?",categories="Programming Functions;Predicates"
*CMD EvenFunction? --- Return true if function is an even function (False otherwise)

*STD
*CALL
        EvenFunction?(expression,variable)

*PARMS

{expression} -- mathematical expression
{variable} -- variable

*DESC

This function returns True if MathPiper can determine that the
function is even. Even functions are
defined to be functions that have the property:

$$ f(x) = f(-x) $$

{Cos(x)} is an example of an even function.

As a side note, one can decompose a function into an
even and an odd part:

$$ f(x) = f_even(x) + f_odd(x) $$

Where 

$$ f_even(x) = (f(x)+f(-x))/2 $$

and

$$ f_odd(x) = (f(x)-f(-x))/2 $$

*E.G.

In> EvenFunction?(Cos(b*x),x)
Result: True

In> EvenFunction?(Sin(b*x),x)
Result: False

In> EvenFunction?(1/x^2,x)
Result: True

In> EvenFunction?(1/x,x)
Result: False

*SEE OddFunction?, Sin, Cos
%/mathpiper_docs