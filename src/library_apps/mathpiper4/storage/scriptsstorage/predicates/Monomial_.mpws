%mathpiper,def="Monomial?;CanBeMonomial"

//Retract("CanBeMonomial",*);
//Retract("Monomial?",*);

10 # CanBeMonomial(_expr)_(Type(expr)=?"UniVariate") <-- False;

10 # CanBeMonomial(_expr)<--Not? (HasFunction?(expr,ToAtom("+")) Or? HasFunction?(expr,ToAtom("-")));

10 # Monomial?(expr_CanBeMonomial) <-- 
{
    Local(r);
    Decide( RationalFunction?(expr),
        r := (VarList(Denominator(expr)) =? []),
        r := True
    );
};

15 # Monomial?(_expr) <-- False;

%/mathpiper








%mathpiper_docs,name="Monomial?",categories="Programming Functions;Predicates"
*CMD Monomial? --- determine if [expr] is a Monomial
*STD
*CALL
        Monomial?(expr)

*PARMS
{expr} -- an expression

*DESC
This function returns {True} if {expr} satisfies the definition of a {Monomial}.
Otherwise, {False}.
A {Monomial} is defined to be a single term, consisting of a product of numbers
and variables.

*E.G.

In> Monomial?(24)
Result: True


In> Monomial?(24*a*x^2*y^3)
Result: True


In> Monomial?(24*a*x^2*y^3/15)
Result: True


In> Monomial?(24*a*x^2*y^3/15+1)
Result: False
    
%/mathpiper_docs
